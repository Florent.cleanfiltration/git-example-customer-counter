
def counter_sum(values):
    return sum(values)


def counter_avg(values):
    return sum(values)/len(values)


def counter_main_process():

    values = []

    while True:
        try:
            user_input = int(input("Entrée votre valeur: "))
        except ValueError:
            # handle if the user try to put letter instead of number
            print("Error: Veillez entrer un nombre")

        if user_input == -1:
            # exit process
            break
        elif user_input == -2:
            # display the sum
            print(f"La somme est {counter_sum(values)}")
        elif user_input == -8:
            # display the average
            print(f"La moyenne est {counter_avg(values)}")
        elif user_input > 0:
            values.append(user_input)

            fre
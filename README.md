Simple exercice pour la présentation de Git  
Les consignes sont dans la suite

1. Demande du client :
  - [ ] J’ai besoin d’un programme qui enregistre des données positive puis affiche le résultat de leur somme.
  - [ ] Merci de fournir un programme avec des tests afin d’éviter des réaparition de bugs  
> Retour du client :  
> Les nombres négatifs sont prix en compte. Je ne veux que sauvegarder que des nombre positif

2. Demande du client :
  - [ ] Je n’aime pas le "Entrée votre valeur :". Changé le vers "Entrée votre nombre :"
3. Demande du client :
  - [ ] J’ai besoin de la moyenne maintenant
4. Demande du client :
  - [ ] En réalité je préférais "valeur".
